use moviedb;

create TABLE actor(
	act_id INT NOT NULL,
    act_fname CHAR(20) NOT NULL,
    act_lname CHAR(20) NOT NULL,
    act_gender CHAR(1) NULL,
    CONSTRAINT actor_pk PRIMARY KEY(act_id)
)

create TABLE director(
	dir_id INT NOT NULL,
    dir_fname CHAR(20) NOT NULL,
    dir_lname CHAR(20) NOT NULL,
    CONSTRAINT director_pk PRIMARY KEY(dir_id)
)

create TABLE movie(
	mov_id INT NOT NULL,
    mov_title CHAR(50) NOT NULL,
    mov_year INT NULL,
    mov_time INT NULL,
    mov_lang CHAR(50) NULL,
    mov_dt_rel DATE,
    mov_rel_country CHAR(5) NULL,
    CONSTRAINT movie_pk PRIMARY KEY(mov_id)
)

create TABLE genre(
	gen_id INT NOT NULL,
    gen_title CHAR(20) NOT NULL,
    CONSTRAINT genre_pk PRIMARY KEY(gen_id)

)

create TABLE reviewer(
	rev_id INT NOT NULL,
    rev_name CHAR(30) NULL,
    CONSTRAINT reviewer_pk PRIMARY KEY(rev_id)
)

create TABLE movie_direction(
	dirId INT NOT NULL,
    movId INT NOT NULL,
	CONSTRAINT director_fk FOREIGN KEY(dirId) REFERENCES director(dir_id),
    CONSTRAINT movie_fk FOREIGN KEY(movId) REFERENCES movie(mov_id)
)

create TABLE movie_cast(
	role CHAR(30) NULL,
    actorId INT NOT NULL,
    movieId INT NOT NULL,
    CONSTRAINT actor_fk FOREIGN KEY(actorId) REFERENCES actor(act_id),
    CONSTRAINT mc_movie_fk FOREIGN KEY(movieId) REFERENCES movie(mov_id)
)

create TABLE movie_genres(
	genId INT NOT NULL,
    movId INT NOT NULL,
	CONSTRAINT mg_genre_fk FOREIGN KEY(genId) REFERENCES genre(gen_id),
    CONSTRAINT mg_movie_fk FOREIGN KEY(movId) REFERENCES movie(mov_id)
)

create TABLE rating(
	rev_stars NUMERIC(2,1) NULL,
    num_o_ratings INT NOT NULL,
    movId INT NOT NULL,
    revId INT NOT NULL,
    CONSTRAINT rt_movie_fk FOREIGN KEY(movId) REFERENCES movie(mov_id),
    CONSTRAINT rt_rev_fk FOREIGN KEY(revId) REFERENCES reviewer(rev_id)
)