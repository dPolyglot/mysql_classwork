show tables;

select mov_title as MovieNames, mov_year as MovieYear from movie;

select mov_year, mov_title from movie where mov_title = 'American Beauty';

select * from movie where mov_year = 1999;

select * from movie where mov_year < 1998;

select * from movie where mov_year < 1999;

select rev_name from reviewer 
union
select mov_title from movie;

select DISTINCT rev_name 
from reviewer, rating 
where rev_stars >= 7.0 and rating.revId = rating.revId;

select DISTINCT movie.mov_title 
from rating, movie 
where rating.num_o_ratings is NULL 
and rating.movId = movie.mov_id;

select DISTINCT movie.mov_title 
from rating, movie 
where rating.rev_stars is NULL 
and rating.movId = movie.mov_id;

select * from movie;

select mov_title, dir_fname, dir_lname, director.dir_id
from 
movie, director, movie_direction
where 
mov_title = 'Eyes Wide Shut' 
and
movie_direction.dirId = director.dir_id
and
movie_direction.movId = movie.mov_id